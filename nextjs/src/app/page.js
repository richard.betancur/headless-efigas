import Posts from "@/components/Posts";
import NavBar from "@/components/Navbar"
export default function Home() {
  return (
    <div>
      <NavBar />
      <Posts/>
    </div>
  );
}
